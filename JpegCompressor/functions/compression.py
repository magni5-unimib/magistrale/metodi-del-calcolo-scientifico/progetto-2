
import math
import os
import shutil

import numpy
from scipy.fft import dctn, idctn

import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def compress(image, F, d):
    h = image.shape[0]
    w = image.shape[1]

    if h%F != 0:
        h = int(h / F) * F
    if w%F != 0:
        w = int(w / F) * F
    image_to_compress = image[0:h, 0:w]

    compressed_image = numpy.zeros((h, w))

    for x in range(0, h, F):
        for y in range(0, w, F):
            block = image_to_compress[x:x+F, y:y+F]

            block = dctn(block, type = 2, norm = 'ortho')

            for i in range(0, F):
                for j in range(0, F):
                    if i + j >= d:
                        block[i, j] = 0
        
            block = idctn(block, type = 2, norm = 'ortho')

            for i in range(0, F):
                for j in range(0, F):
                    value = numpy.round(block[i, j])
                    if value < 0:
                        value = 0
                    elif value > 255:
                        value = 255
                    block[i, j] = value

            compressed_image[x:x+F, y:y+F] = block

    return compressed_image