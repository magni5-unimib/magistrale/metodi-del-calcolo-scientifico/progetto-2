
import math

import numpy 


def dct(x):
    c = numpy.zeros(x.size)
    N = x.size
    for k in range(N):
        sum = 0
        a = math.sqrt(1. / N) if k == 0 else math.sqrt(2. / N)
        for i in range(N):
            sum += x[i] * math.cos((k * math.pi * (2 * i + 1)) / (2 * N))
        c[k] = a * sum
    return c

def dct2(x):
    c = numpy.zeros(x.shape)
    c = numpy.apply_along_axis(dct, axis = 1, arr = x)
    c = numpy.apply_along_axis(dct, axis = 0, arr = c)
    return c