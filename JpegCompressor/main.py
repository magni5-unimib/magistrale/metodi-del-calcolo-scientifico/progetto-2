
import gi
gi.require_version("Gtk", "4.0")
from gi.repository import GLib, Gtk

import sys

from windows.home import Home


class MyApplication(Gtk.Application):

    def __init__(self):
        super().__init__(application_id="it.magni5.jpegcompressor")
        GLib.set_application_name('Jpeg compressor')

    def do_activate(self):
        win = self.props.active_window
        if not win:
            win = Home(application=self)
        win.present()


app = MyApplication()
exit_status = app.run(sys.argv)
sys.exit(exit_status)