
from gi.repository import Gtk, Gio

from .part1 import Part1
from .part2 import Part2


class Home(Gtk.ApplicationWindow):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.build_ui()

    def build_ui(self):
        self.set_default_size(500, 500)

        # Main window
        window = Gtk.Box(
            orientation = Gtk.Orientation.VERTICAL,
            margin_top = 10, 
            margin_bottom = 10,
            margin_start = 10, 
            margin_end = 10,
            spacing = 10
        )

        # Description label
        description = Gtk.Label(
            label = 'Choose the part you want to open.'
        )
        window.append(description)

        # Part 1 button
        button = Gtk.Button(label = 'Part 1')
        window.append(button)
        button.connect('clicked', self.open_part1)

        # Part 2 button
        button = Gtk.Button(label = 'Part 2')
        window.append(button)
        button.connect('clicked', self.open_part2)

        self.set_child(window)

    def open_part1(self, button):
        part1 = Part1()
        part1.present()

    def open_part2(self, button):
        part2 = Part2()
        part2.present()