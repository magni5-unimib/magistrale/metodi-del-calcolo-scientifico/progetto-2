
from gi.repository import Gtk, Gio

import time
import threading

import numpy
from scipy import fft
import matplotlib
matplotlib.use('GTK4Cairo') 
import matplotlib.pyplot as plt
from matplotlib.backends.backend_gtk4agg import (
    FigureCanvasGTK4Agg as FigureCanvas
)
from matplotlib.figure import Figure
import pandas

from functions import homemade
from utils import settings


class Part1(Gtk.ApplicationWindow):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.build_ui()

    def build_ui(self):
        self.set_default_size(500, 500)

        # Main window
        window = Gtk.Box(
            orientation = Gtk.Orientation.VERTICAL,
            margin_top = 10, 
            margin_bottom = 10,
            margin_start = 10, 
            margin_end = 10,
            spacing = 10
        )

        # Description label
        description = Gtk.Label(
            label = 'Choose the function you want to execute.'
        )
        window.append(description)

        # DCT
        button = Gtk.Button(label = 'Home-made DCT test')
        window.append(button)
        button.connect('clicked', self.dct_test)

        # DCT2
        button = Gtk.Button(label = 'Home-made DCT2 test')
        window.append(button)
        button.connect('clicked', self.dct2_test)

        # Comparison
        button = Gtk.Button(label = 'Compare execution times')
        window.append(button)
        button.connect('clicked', self.comparison_async)

        # Log label
        self.log_label = Gtk.Label()
        self.log_scrolledwindow = Gtk.ScrolledWindow()
        window.append(self.log_scrolledwindow)
        self.log_scrolledwindow.set_child(self.log_label)

        # Plot
        self.fig = Figure()
        canvas = FigureCanvas(self.fig)
        window.append(canvas)

        self.set_child(window)

    def dct_test(self, button):
        self.log('Start dct_test')

        matrix = numpy.array(settings.TEST_MATRIX)
        homemade_matrix = homemade.dct(matrix[0,:])
        scipy_matrix = fft.dct(matrix[0,:], norm = 'ortho')
        numpy.savetxt(
            settings.OUTPUT + 'homemade_dct.csv', 
            homemade_matrix, 
            delimiter = ',', 
            fmt = '%.2e'
        )
        numpy.savetxt(
            settings.OUTPUT + 'scipy_dct.csv', 
            scipy_matrix, 
            delimiter=',', 
            fmt='%.2e'
        )

        
        self.log('End dct_test')

    def dct2_test(self, button):
        self.log('Start dct2_test')

        matrix = numpy.array(settings.TEST_MATRIX)
        homemade_matrix = homemade.dct2(matrix)
        # scipy_matrix = fftpack.dct(
        #     fftpack.dct(
        #         matrix, 
        #         axis = 0, 
        #         norm = 'ortho'
        #     ), 
        #     axis = 1, 
        #     norm = 'ortho'
        # )
        scipy_matrix = fft.dctn(matrix, norm = 'ortho')
        numpy.savetxt(
            settings.OUTPUT + 'homemade_dct2.csv', 
            homemade_matrix, 
            delimiter = ',', 
            fmt = '%.2e'
        )
        numpy.savetxt(
            settings.OUTPUT + 'scipy_dct2.csv', 
            scipy_matrix, 
            delimiter=',', 
            fmt='%.2e'
        )

        self.log('End dct2_test')

    def comparison_async(self, button):
        threading.Thread(
            target = self.comparison, args = [button]
        ).start()

    def comparison(self, button):
        self.log('Start comparison')

        homemade_times = []
        scipy_times = []

        dimensions = settings.DIMENSIONS

        for dimension in dimensions:
            self.log(f'Start processing: {dimension}.')

            matrix = numpy.random.randint(
                low = 0, 
                high = 255, 
                size = (dimension, dimension)
            )

            start1 = time.time()
            homemade.dct2(matrix)
            end1 = time.time() - start1
            homemade_times.append(end1)

            start2 = time.time()
            fft.dctn(matrix, norm = 'ortho')
            end2 = time.time() - start2
            scipy_times.append(end2)

            self.log(f'End processing: {dimension}.')

        # Plot
        plt = self.fig.add_subplot()
        plt.plot(
            dimensions, 
            homemade_times, 
            label = 'homemade DCT2', 
            marker = 'o')
        plt.plot(
            dimensions, 
            scipy_times, 
            label = 'scipy\'s DCT2', 
            marker = 'o')
        plt.set_xlabel('Matrix dimensions')
        plt.set_ylabel('Time (s)')
        plt.legend()
        self.fig.savefig(settings.OUTPUT + 'plot.png')
        self.log('Plot generated and exported (plot.png).')

        # Times
        df = pandas.DataFrame({
            'dimensions': dimensions, 
            'homemade DCT2 (s)': homemade_times, 
            'scipy\'s DCT2 (s)':scipy_times
        })
        df.to_csv(settings.OUTPUT + 'times.csv')
        self.log('Execution times comparison table exported (times.csv).')
                
        self.log('End comparison')

    def log(self, text):
        self.log_label.set_text(self.log_label.get_text() + '\n' + text)
        self.log_scrolledwindow.set_placement(Gtk.CornerType.BOTTOM_LEFT)