
from gi.repository import Gtk, Gio, GLib

import threading

import matplotlib
matplotlib.use('GTK4Cairo') 
import matplotlib.pyplot as plt
from matplotlib.backends.backend_gtk4agg import (
    FigureCanvasGTK4Agg as FigureCanvas)
from matplotlib.figure import Figure
import matplotlib.image as mpimg
import cv2

from functions import compression


class Part2(Gtk.ApplicationWindow):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.build_ui()

    def build_ui(self):
        self.set_default_size(500, 500)

        # Main window
        window = Gtk.Box(
            orientation = Gtk.Orientation.VERTICAL,
            margin_top = 10, 
            margin_bottom = 10,
            margin_start = 10, 
            margin_end = 10,
            spacing = 10
        )

        # Description label
        description = Gtk.Label(
            label = 'Set parameters and start the compression.'
        )
        window.append(description)

        # File picker
        button = Gtk.Button(label = 'Choose a bitmap image')
        window.append(button)
        button.connect('clicked', self.show_file_dialog)
        self.file_dialog = Gtk.FileChooserNative.new(
            title = 'Choose a bitmap image',
            parent = self, 
            action = Gtk.FileChooserAction.OPEN
        )
        self.file_dialog.connect('response', self.file_dialog_response)
        # I want only bitmap files
        # ...but it doesn't work with FileChooserNative
        # f = Gtk.FileFilter()
        # f.set_name("Bitmap images")
        # f.add_mime_type("image/bmp")
        # filters = Gio.ListStore.new(Gtk.FileFilter)
        # filters.append(f)
        # self.file_dialog.set_filters(filters)
        # self.file_dialog.set_default_filter(f)
        self.file_label = Gtk.Label(
            label = '...'
        )
        window.append(self.file_label)

        # F parameter
        label = Gtk.Label(
            label = 'F parameter'
        )
        window.append(label)
        self.F_entry = Gtk.Entry()
        window.append(self.F_entry)

        # d parameter
        label = Gtk.Label(
            label = 'd parameter'
        )
        window.append(label)
        self.d_entry = Gtk.Entry()
        window.append(self.d_entry)

        # Compress
        button = Gtk.Button(label = 'Compress image')
        window.append(button)
        button.connect('clicked', self.compression_async)

        # Images
        self.fig = Figure()
        canvas = FigureCanvas(self.fig)
        window.append(canvas)

        self.set_child(window)
        
    def show_file_dialog(self, button):
        self.file_dialog.show()
        
    def file_dialog_response(self, dialog, response):
        try:
            if response == Gtk.ResponseType.ACCEPT:
                file = dialog.get_file()
                if file is not None:
                    self.file_label.set_text(file.get_basename())
            else:
                print('Closed dialog')
        except GLib.Error as error:
            print(f'Error opening file: {error.message}')

    def compression_async(self, button):
        threading.Thread(
            target = self.compression, args = [button]
        ).start()

    def compression(self, button):
        filename = self.file_dialog.get_file().get_basename()
        filepath = self.file_dialog.get_file().get_path()
        F = int(self.F_entry.get_text())
        d = int(self.d_entry.get_text())

        img_to_compress = cv2.imread(filepath, 0)

        # Compress image
        compressed_img = compression.compress(img_to_compress, F, d)

        # Save compressed image
        output_filename = f'output/{filename}_{str(F)}_{str(d)}.png'
        plt.imsave(output_filename, arr = compressed_img, cmap = 'gray')

        # Show both images
        o = self.fig.add_subplot(1, 2, 1)
        c = self.fig.add_subplot(1, 2, 2)
        o.set_title('Original image')
        o.imshow(img_to_compress, cmap = 'gray')
        c.set_title('Compressed image')
        c.imshow(compressed_img, cmap = 'gray')