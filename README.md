# Progetto 2

Part 1 : Implementation of a home-made DCT2 in Python and comparison with one of a known library in its FFT version.

Part 2 : Implementation of a GTK4 Python GUI to compress a bitmap image using DCT2.

## How-to

Bash commands to set up python virtual environment and install needed libraries.

The last command executes the main script.

```bash
cd JpegCompressor
pyhton -m venv env
source env/bin/activate
pip install -r requirements.txt
pyhton main.py
```

## Report

Check out the [Report](Report/Report.pdf) for more.